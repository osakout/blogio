const mysql = require('mysql');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "blog",
  port: "3306",
  socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

module.exports = con;