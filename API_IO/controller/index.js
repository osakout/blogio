const con = require('../config/db');

class ArticleController {

    welcome(req, res, next) {
        return res.status(200).send({code:200, message: "OK", body: "API BLOG IO"})
    }
    allArticles(req, res, next) {
		con.query(" SELECT * FROM `articles` ORDER BY `ID` DESC ", function(error, articles, fields) {
			if (error) throw error;
			let article = articles[0]

/* 			function ConvertDate (dateTime) {

				var date = dateTime.split('-');

				date[2] = date[2].substr(0, 2);
				date = date.join('-');

				return date;
			}

			console.log(ConvertDate(articles[1].date)) */

			return res.status(200).send({code: 200, message: "OK", datas: articles});
		})
    }
    oneArticles(req, res, next) {
		con.query(" SELECT * FROM `articles` WHERE ID='"  + req.params.id +  "' ", function(error, articles, fields) {
			if (error) throw error;
			let article = articles[0]
			return res.status(200).send({code: 200, message: "OK", datas: articles});
		})
		}
		newArticles(req, res, next) {
			con.query(" SELECT * FROM `articles` ORDER BY `ID` DESC LIMIT 3 ", function(error, articles, fields) {
				if (error) throw error;
				let article = articles[0]
				return res.status(200).send({code: 200, message: "OK", datas: articles});
		})
		}
    	postArticle(req, res, next) {
			con.query(" INSERT INTO `articles` (`titre`, `body`, `auteur`, `date`, `YoutubeURL`) VALUES ('" + req.body.titre + "', '" + req.body.body + "', '" + req.body.auteur + "', DATE(NOW()), '" + req.body.YoutubeURL + "' ) ", function(error, articles, fields) {
				if (error) throw error;

				return res.status(201).send({code: 201, message: "Created", datas : { ID: articles.insertId, titre: req.body.titre , body: req.body.body , auteur: req.body.auteur, YoutubeURL: req.body.YoutubeURL}});
		})
		}
		putArticle(req, res, next) {
			con.query(" UPDATE `articles` SET titre='" + req.body.titre + "', body='" + req.body.body + "', auteur='" + req.body.auteur + "' WHERE ID='" + req.body.ID + "' ", function(error, articles, fields) {
				if (error) throw error;

				return res.status(200).send({code: 201, message: "Modified", datas : { ID: articles.insertId, titre: req.body.titre , body: req.body.body , auteur: req.body.auteur}});
			})
			}
			deleteArticles(req, res, next) {
				con.query(" DELETE FROM `articles` WHERE ID='"  + req.headers.id +  "' ", function(error, articles, fields) {
					if (error) throw error;
					let article = articles[0]
					
					return res.status(200).send({code: 200, message: "Deleted", datas: articles});
				})
				}
    noRoute(req, res, next) {
        return res.status(404).send({code: 404, message: "No route"});
    }

}

module.exports = new ArticleController();