const express = require('express');
const bodyParser = require("body-parser"); 
const app = express();
const cors = require('cors');
const myRouter = require('./route/');
const hostname = 'localhost';
const port = '8080';

const con = require('./config/db');

try {
  con.connect(function(){
    console.log("Connection à la base de données réussi!");
  });
} catch (error) {
  console.error(error);
}

/*
con.connect(function(err) {
  if (err) throw err;
  console.log("Connection à la base de données réussi!");
}).catch(err);
*/
/* app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}) */
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//On demande à l'application d'utiliser notre router

app.use(myRouter);

//Démarrage du serveur

app.listen(port, hostname, function(){
	console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});