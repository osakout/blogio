const express = require('express');
const bodyParser = require("body-parser");
const myRouter = express.Router();
const controller = require('../controller/index');

myRouter.route("/articles")
.get(controller.allArticles);

myRouter.route("/articles/:id")
.get(controller.oneArticles);

myRouter.route("/newArticles")
.get(controller.newArticles);

myRouter.route("/article")
.post(controller.postArticle);

myRouter.route("/updateArticle")
.put(controller.putArticle);

myRouter.route("/deleteArticle")
.delete(controller.deleteArticles);

myRouter.route("/")
.get(controller.welcome);

myRouter.route("*")
.get(controller.noRoute);

module.exports = myRouter;
