'use strict';

// Define the `phonecatApp` module
angular.module('phonecatApp', [
  // ...which depends on the `phoneList` module
  'articleList',
  'articlePost',
  'articleDetail',
  'newArticle',
  'articleUpdate',
  'navBar',
  'imageUpload',
  'ngRoute'
]);
