'use strict'

angular.module('imageUpload').directive('fileModel', ['$parse', function ($parse) {
   return {
      restrict: 'A',
      link: function(scope, element, attrs) {
         var model = $parse(attrs.fileModel);
         var modelSetter = model.assign;
         
         element.bind('change', function() {
            scope.$apply(function() {
               modelSetter(scope, element[0].files[0]);
            });
         });
      }
   };
}]);
angular.module('imageUpload').service('fileUpload', ['$http', function ($http) {
   this.uploadFileToUrl = function(file, uploadUrl) {
      var fd = new FormData();
      fd.append('file', file);
   
      $http.post(uploadUrl, fd, {
         transformRequest: angular.identity,
         headers: {'Content-Type': undefined, 'Access-Control-Allow-Origin': '*','Accept': '*', 'Allow': 'POST'}
      }).then(
          function successCallBack(response){
              console.log('success upload')
          }, function errorCallback(response){
              console.log('failed upload')
              console.log(response)
          })
   }
}]);
angular.module('imageUpload').controller('myCtrl', ['$scope', 'fileUpload', function($scope, fileUpload) {
   $scope.uploadFile = function() {
      
      var file = $scope.myFile;
      console.log('file is ' );
      console.dir(file);
      var uploadUrl = "/fileUpload";
      fileUpload.uploadFileToUrl(file, uploadUrl);
   };
}]);
angular.module('imageUpload').component('imageUpload', {
    templateUrl: 'image-upload/image-upload.template.html',

})
