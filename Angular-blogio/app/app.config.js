'use strict';

angular.
  module('phonecatApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/articles', {
          template: '<article-list></article-list>'
        }).
        when('/articles/:articleID', {
            template: '<article-detail></article-detail>'
        }).
        when('/updateArticles/:articleID', {
          template: '<article-update></article-update>'
        }).
        when('/addArticle', {
          template: '<article-post></article-post>'
        }).
        when('/fileUpload', {
          headers: {
            'Content-Type': undefined, 'Access-Control-Allow-Origin': '*','Accept': '*', 'Allow': 'POST'
          }
        }).
        otherwise('/articles');
    }
  ]);
