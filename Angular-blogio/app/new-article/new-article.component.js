'use strict';

angular.
  module('newArticle').
  component('newArticle', {
    templateUrl: 'new-article/new-article.template.html',
    controller: ['$http', function NewArticleController($http) {
      var self = this;

      $http.get('http://localhost:8080/newArticles').then(function(response) {
        self.articles = response.data.datas;
      });
    }]
  })