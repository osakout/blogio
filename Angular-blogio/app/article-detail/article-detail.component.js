'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('articleDetail').
  component('articleDetail', {
    templateUrl: 'article-detail/article-detail.template.html',
    controller: ['$http','$routeParams', '$location', '$sce', function ArticleDetailController($http, $routeParams, $location, $sce) {
      var self = this;

      $http.get('http://localhost:8080/articles/'  + $routeParams.articleID +  '').then(function(response) {
        self.articles = response.data.datas[0];
        self.putArticle = function putArticle() {
          console.log('submited')
          $location.path('/updateArticles/' + self.articles.ID + '')
        }

        self.lien = self.articles.YoutubeURL;
        function getId(url) {
          var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
          var match = url.match(regExp);
      
          if (match && match[2].length == 11) {
              return match[2];
          } else {
              return 'error';
          }
      }
      
      var myId = getId(self.lien);
      if (self.lien != "")
      {
              $('#myId').html(myId);
      
      $('#myCode').html('<div class="embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen></iframe></div>');
      }

      });
    }]
  });
