'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('articleUpdate').
  component('articleUpdate', {
    templateUrl: 'article-update/article-update.template.html',
    controller: ['$http', '$routeParams', '$location', function ArticleUpdateController($http, $routeParams, $location) {
      var self = this;

      $http.get('http://localhost:8080/articles/'  + $routeParams.articleID +  '').then(function(response) {
          self.articles = response.data.datas[0];
        });

      self.putArticle = function () {
          $http.put('http://localhost:8080/updateArticle', {titre: self.articles.titre, body: self.articles.body, auteur: self.articles.auteur, ID: self.articles.ID})
          .then(function successCallback(response){
              console.log("Successfully POST-ed data");
              self.posted = 'Successfully POST-ed data';
              console.log(self.articles.titre)
              $location.path('/articles/' + self.articles.ID + '')
            }, function errorCallback(response){
                console.log("POST-ing of data failed");
                self.posted = 'Successfully POST-ed data';
            }); 
        }
        
        self.deleteArticle = function () {
            $http({  
                method: "DELETE",  
                url: "http://localhost:8080/deleteArticle",  
                headers: {'ID': self.articles.ID }  
            }).then(function(response) {
                $location.path('/articles')
            })
        }
    
    }]
});
