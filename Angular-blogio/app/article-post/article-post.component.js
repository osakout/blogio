'use strict';

angular.
  module('articlePost').
  component('articlePost', {
    templateUrl: 'article-post/article-post.template.html',
    controller: ['$http','$location', function ArticlePostcontroller($http, $location) {
      var self = this;

      self.titre = "";
      self.auteur = "";
      self.body = "";
      self.YoutubeURL = "";

      self.postArticle = function () {
        $http.post('http://localhost:8080/article', {titre: self.titre, body: self.body, auteur: self.auteur, YoutubeURL: self.YoutubeURL})
        .then(function successCallback(response){
          console.log("Successfully POST-ed data");
          console.log(self.YoutubeURL)
          self.posted = 'Successfully POST-ed data';
          $location.path('/articles/' + response.data.datas.ID + '')
        }, function errorCallback(response){
          console.log("POST-ing of data failed");
          self.posted = 'Successfully POST-ed data';
        });

      }
    }]
  })
