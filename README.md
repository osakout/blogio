---
**Title**:          Blog IO
**Author**:         Omar Sakout
**Date**:           24/11/2018
---

#BLOGIO

BlogIO est un blog sous AngularJS

---

##Fonctionnalités

__Fonctionnalités du blog :__

* Création d'articles
* Recherche d'articles
* Tri d'article
* Liste de tous les articles
* 3 derniers articles ajouter
* Modification / Suppression article
* Possibiliter d'ajouter une vidéo Youtube à son article
* Article: titre, contenu, auteur, date, YoutubeURL

##PREREQUIS
---

Avant toute chose, veillez à modifier la configuration de la base de données de l'API situé dans /API_IO/conifg/db.js avec la configuration de votre base de données.

Vous est fournis un dump du Blog que vous pourrez importer dans votre base de données SQL. /dump/blog.sql

###Schema table sql

{
    **ID**: int,
    **titre**: text,
    **body**: text,
    **auteur**: text,
    **date**: date,
    **YoutubeURL**: text
}

##INSTALLATION
---

Pour installer l'API, lancez le script *install_API_IO* situé dans le dosier */install*

    ./install_API_IO

Pour installer le FRONT, lancez le script *install_Angular-blogio* situé dans le dosier */install*

    ./install_Angular-blogio

*Ce qui aura pour effet d'installer respectivement les dépendences de l'API et d'AngularJS*

##LANCEMENT
---

Après l'installation de toute les dépendences, lancez le script *startup_API_IO* situé dans la racine du projet *blogio* pour pouvoir lancer l'API, depuis un terminal

    ./startup_API_IO

Ensuite lancez le front Angular avec le script *startup_Angular-blogio* situé aussi dans la racine du projet *blogio*, depuis un nouveau terminal

    ./startup_Angular-blogio

Vous pourrez alors accédez au Blog depuis ce lien (http://localhost:8000/)

[BLOG IO CLIQUEZ ICI](http://localhost:8000/)

##ARRÊT DE L'APPLICATION
---

Pour pouvoir arrêter l'application, faites un  **ctrl + c**  sur chaque terminal où chaque instance est lancé
